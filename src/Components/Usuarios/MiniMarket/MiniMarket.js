import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "./MiniMarket.css";

export default function MiniMarket() {

  const coleccionCosas = JSON.parse(localStorage.getItem('CarritoItems')) || [];
  const [featured, setFeatured] = useState({
    prodName: '',
    image: '',
    discount: '',
    price: '',
  })
  const [itemAmount, setItemAmount] = useState(1);
  const [its, setIts] = useState();

  
  useEffect(() => {
    let status;
    fetch("http://localhost:4000/products/search/allFeatured")
      .then(resp => {
        if (!resp.ok) status = resp.status;
        return resp.json();
      })
      .then(resp => {
        if (status) {
          console.log(resp);
          return;
        }
        setFeatured(resp);
      });
  }, []);


  const handleInput = e => {
    const value = e.target.value;
    setItemAmount(value);
  };

  function handleClick(id) {
    let resultadoFind = featured.find(item => {
      return item._id === id;
    });
    resultadoFind.quantity = itemAmount;
    coleccionCosas.push(resultadoFind);
    save(coleccionCosas);
    setIts(coleccionCosas);
  }

  function save(it) {
    localStorage.setItem("CarritoItems", JSON.stringify(it));
  }

  const handleDeleteCart = (id) =>{
    let coleccionCosas = JSON.parse(localStorage.getItem("CarritoItems")) || [];
    let exist = coleccionCosas.filter(item => {
      return item._id !== id;
    });
    localStorage.setItem("CarritoItems", JSON.stringify(exist));
    setIts(JSON.parse(localStorage.getItem("CarritoItems")));
    console.log(its);
  }

  function isAdded(id) {
    let exist = coleccionCosas.find(item => {
      return item._id === id;
    });

    return exist ? true : false;
  }

  const Destacados = Array.isArray(featured) && featured.map((item, i) => {
    let a = "";
    if (isAdded(item._id)) {
      a = (
        <button onClick={() =>{handleDeleteCart(item._id)}} className="slide-top border-0 p-0 bg-white">
          <img  alt="icono agregado"
            title="Item Added to Cart :D"
            src="https://img.icons8.com/color/40/000000/checked-2.png"
          />
        </button>
      );
    } else {
      a = (
        <button onClick={() => handleClick(item._id)}
          className="slide-in-bottom align-self-center border-0 p-0 bg-white">
          <img alt="icono agregar al carrito"
            title="Add to cart"
            src="https://img.icons8.com/officel/40/000000/shopping-cart-loaded.png"
          />
        </button>
      );
    }
    return (
      <div className="col-12 col-lg-4 my-3" key={i}>
        {item.discount ? (
          <div className="discount-label yellow">
            {" "}
            <span>-{item.discount}%</span>
          </div>
        ) : (
            ""
          )}
        <div className="card card-style">
          <img  alt="imagen de producto"
            src={item.image}
            className="card-img-top img-fluid align-self-center style-img-items"
          />
          <div className="card-body">
            <div className="row">
              <div className="">
                <Link to={"/article/" + item._id}>
                  <h5 className="card-title text-capitalize">{item.prodName}</h5>
                </Link>
              </div>
              <div className="col-12 d-flex justify-content-between aling-content-center input-group-prepend box-Cart">
                {a}
                <select name="quantity" className="form-control inputNumberButtons w-25 text-center" onChange={handleInput}>
                <option value="1" defaultValue>1</option> 
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
              </select>
              </div>
              <div className="col-12 mt-3">
                <span className="style-price-span box-price">
                  Precio: ${item.price}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  });

  return (
    <section className='container-fluid my-5' id="minimarket">
      <div className='row justify-content-md-between justify-content-center'>
        <div className='col-12 col-md-8'>
          <div className='row justify-content-around  container-card'>
            {Destacados}
          </div>
          <div className=" d-flex justify-content-center mt-3">
            <Link to="/market" className="btn btn-info btn-block">
              Ver Mas...
            </Link>
          </div>
        </div>
        <div className="col-12 col-md-4 mt-5 d-flex align-items-around justify-content-center">
          <div className="row">
            <div className="main-publi-mp col-10">
              <img alt="banner mercadopago"
                src="images/mercadopagopic.png"
                className="card-img-top img-fluid w-100"
              />
            </div>
            <div className="main-publi publi-content col-10">
              <h2 className="">EMERGENCIAS 24HS</h2>
              <img alt="imagen emergencias"
                src="images/dogsemergency.gif"
                className="card-img-top img-fluid"
              />
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
