import React from "react";
import "./Slider.css";
import { Link } from "react-router-dom";

export default class Slider extends React.Component {

  render() {
    return (
      <React.Fragment>
        <div
          id="carouselExampleInterval"
          className="carousel slide d-none d-md-block d-lg-block d-xl-block"
          data-ride="carousel"
          data-pause="false"
          data-interval="4000"
        >
          <div className="carousel-inner carousel-size">
            <div className="carousel-item active py-3">
              <div className="urgenciasdiv text-center" id="slider">
                <h2>
                  <span>EMERGENCIAS</span> <span>VETERINARIAS</span>{" "}
                </h2>
                <h3> 24 Hs 03456722-43212344</h3>
              </div>
              <img
                alt="imagen emergencias"
                src="images/sliderban.png"
                className="d-block w-100 img-responsive "
              />
            </div>
            <div className="carousel-item py-3" data-interval="3000">
              <div className="peluqueriadiv banner-text">
                <h3>PELUQUERIA</h3>
                <Link to="/Turnos">
                  <button type="button" className="btn sharedbtn-sli ml-5 ">
                    <span>Turnos</span>{" "}
                  </button>
                </Link>
              </div>
              <img
                src="images/pelu.png"
                className=" w-100 img-responsive"
                alt="..."
              />
            </div>
            <div className="carousel-item py-3" data-interval="3000">
              <div className="petshopdiv text-center banner-text">
                <h3>NUESTROS PRODUCTOS</h3>
                <div className="btnslideprod">
                  <Link to="/Minimarket">
                    <button type="button" className="btn sharedbtn-sli ml-5 ">
                      <span>Ver...</span>{" "}
                    </button>
                  </Link>
                </div>
              </div>
              <img
                alt="imagen nuestros productos"
                src="images/sliderpet.png"
                className="d-block w-100 img-responsive"
              />
            </div>
          </div>
          <a
            className="carousel-control-prev"
            href="#carouselExampleInterval"
            role="button"
            data-slide="prev"
          >
            <span
              className="carousel-control-prev-icon fas fa-chevron-circle-left mr-5 ml-n5"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Previous</span>
          </a>
          <a
            className="carousel-control-next"
            href="#carouselExampleInterval"
            role="button"
            data-slide="next"
          >
            <span
              className="carousel-control-next-icon fas fa-chevron-circle-right ml-5 mr-n5"
              aria-hidden="true"
            ></span>
            <span className="sr-only">Next</span>
          </a>
        </div>
      </React.Fragment>
    );
  }
}
