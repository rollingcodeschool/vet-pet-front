import React from "react";
import "./NavLinksBar.css";
import { HashLink as Link } from "react-router-hash-link";

export default class NavLinksBar extends React.Component {
 
  render() {
    return (
      <div className="d-flex justify-content-md-center">
        <nav className="navbar navbar-expand-md navbar-light align-items-center mt-1">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            {/* <div>{this.userLogged()}</div> */}
            <div className="nav-dir flex-sm-column ">
              <div className="row shift ">
                <Link className=" nav-item active nav-link" to="/">
                  Home <span className="sr-only">(current)</span>
                </Link>
                <hr></hr>
                <Link className="nav-item nav-link" to={{pathname: "/market", hash:"#market"}}>
                  Market
                </Link>
                <hr></hr>
                <Link   className="nav-item nav-link"  to={"/turnos"}>
                  Turnos 
                </Link>
                <hr></hr>
                <Link className="nav-item nav-link" to={{pathname: "/contacto", hash:"#contacto"}} >
                  Consultas
                </Link>
                <hr></hr>
                <Link className="nav-item nav-link" to={{pathname: "/ourteam", hash:"#ourteam"}}>
                  Nuestro Equipo
                </Link>
                <hr></hr>
                <Link className="nav-item nav-link"  to={{pathname: "/servicios", hash:"#servicios"}}>
                  Servicios
                </Link>
              </div>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}
