import React, { Component } from "react";

export default class ServiciosRuta extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-4">
            <h1>Aqui va el Servicio</h1>
            <img
              className="w-100"
              src="https://d.newsweek.com/en/full/1518384/international-cat-day-oldest-cats.jpg?w=1600&h=1600&q=88&f=4105be22964dbe1e578db68a3c2b33ed"
            ></img>
          </div>
          <div className="col-8">
            <text>
              {" "}
              <h3>Grooming </h3>
              <h5>
                {" "}
                Grooming is always an essential part of your dog or cat’s health
                and happiness. Grooming helps your pet stay in great health,
                keeping them feeling good, looking sharp and smelling fresh!
                Here are just a few of the ways grooming can be beneficial for
                your furry friend: Get shedding under control. Shedding is messy
                and frustrating but can easily be handled with regular grooming
                visits. Our trained professionals know the specifics on how to
                groom your breed of animal. By having your pet groomed regularly
                you can avoid medical problems. When your pet is groomed at
                Helena Veterinary Clinic our vet professionals examine major
                areas like the eyes, ears and nose, checking for signs of
                illness or infection. Keep your pet looking great! Animals feel
                better and look better when they are properly groomed. Avoid
                tangled, matted hair with regular grooming. You may not have the
                time to thoroughly groom your pet. Schedule an appointment with
                Helena Veterinary Clinic, where our veterinary professionals
                have the breed specific knowledge to take great care of your
                furry loved ones and save you time. Grooming Services in Helena,
                Alabama At Helena Veterinary Clinic, we offer a variety of pet
                grooming services, including:
                <ul>
                  <li>
                    {" "}
                    Cuts: When your dog or cat is in need of a trim, we can take
                    care of it with our breed specific cuts. This service will
                    help your pet feel lighter and cooler, keep fur out of their
                    eyes and improve their appearance.
                  </li>
                  <li>
                    Nail Trimming: When your pet's nails get too long, they can
                    curl and cause joint pain in their paws. Nail clipping is
                    part of every grooming visit.{" "}
                  </li>
                  <li>
                    Spa Bath: We give baths with the highest-quality equipment.
                    Our pet bathing service comes with anal gland expression
                    when needed, ear cleaning and nail trimming. We finish with
                    a blow out to make them feel dry and comfortable before
                    heading home. The spa bath may also relieve itchy or
                    irritated skin.
                  </li>
                  <li>
                    {" "}
                    De-shedding Treatments: De-shedding treatments help control
                    an over-abundance of shedding by addressing the root of the
                    problem - the undercoat.
                  </li>
                </ul>
              </h5>{" "}
            </text>
          </div>
        </div>
        <div>
          <button className="btn-primary">Make an appointment!</button>
        </div>
      </div>
    );
  }
}
