# Vet Pet Front

Frontend TFI Rolling Code School
React

# *Lineamientos y procedimientos del PFI FullStack - Veterinaria*


# GIT

En el repositorio se trabajará de la siguiente forma:

1. - **Al comenzar una tarea se debe crear una branch de master.**
2. - **Terminada la tarea, mergear con la rama dev para probar que no haya errores.**
3. - **Si no hay errores, generar un Pull Request para el mergeo con la branch del sprint en curso, para que sea revisada y aprobada.**



# CODIFICACION

Para que cada respectivo Pull Request sea aceptado, deberá cumplir con las siguientes condiciones:

- **Identar con dos espacios todo el código.**
- **Nombrar variables y funciones de forma coherente.**
- **Evitar el uso de var para declarar variables, utilizar const(para constantes, arrays y objetos) y let según corresponda.**
- **Evitar comentarios innecesarios.**
- **NO debe haber console.logs en el código.**

# FRONTEND

- **Para componentes reducidos, utilizar componentes funcionales.**